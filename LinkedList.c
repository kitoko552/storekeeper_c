#include "linkedlist.h"

/**
 * 新しいノードを作成する関数
 * @param int 最初に入れる値
 * @param NODE* 次のノードを指すポインタ
 *
 * @return 新しく生成したノードのポインタ
 */
NODE* newNode(FIELD, NODE*);

NODE* newNode(FIELD field, NODE *next) {
	NODE *node;
	node = malloc(sizeof(NODE));

	if (node == NULL) {
		return NULL;
	}

	node->field.squares = malloc(sizeof(char) * (strlen(field.squares) + 1));

	if (node -> field.squares == NULL) {
		return NULL;
	}

	// squaresは直接代入するとアドレスを渡すことになるのでstrcpyでコピーする。
	strcpy(node -> field.squares, field.squares);
	node->field.playerPosition = field.playerPosition;

	node->next = next;

	return node;
}

bool push(FIELD field, NODE **list) {
	NODE *node;

	node = newNode(field, *list);

	if (node == NULL) {
        return false;
    }

	*list = node;

	return true;
}

bool pop(FIELD *field, NODE **list) {
	NODE *top;

	if (*list == NULL) {
		return false;
	}

	top = *list;
	*list = (*list) -> next;
	*field = top -> field;

	free(top);
	top = NULL;

	return true;
}

bool removeAt(int index, NODE **list) {
	NODE *deleteNode;
	int i;

	if (index < 0) {
		return false;
	}

	for (i = 0; i < index && *list != NULL; i++) {
		list = &((*list) -> next);
	}

	// この時点でiがindexより小さいということは、indexがlist長を超えていることを示す。
	if (i < index) {
		return false;
	}

	deleteNode = *list;
	*list = (*list) -> next;
	free(deleteNode);

	return true;
}

int size(NODE *list) {
	int num = 0;

	while (list != NULL) {
		list = list -> next;
		num++;
	}

	return num;
}

void clearAll(NODE **list) {
	NODE *node;

	while (*list != NULL) {
		node = *list;
		*list = (*list) -> next;
		free(node);
		node = NULL;
	}
}
