#ifndef UTIL_H
#define UTIL_H

#include "soukoban.h"

/**
 * フィールドファイルのディレクトリとファイル名を連結する関数
 *
 * @param char* フィールドディレクトリと連結するファイル名
 *
 * @return 連結後の文字列（パス名）
 */
char* concatDirAndFile(char *);

/**
 * ディレクトリ以下にあるポーズファイルを消す関数
 */
void removePauseFile();

#endif
