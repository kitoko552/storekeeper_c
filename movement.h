#ifndef MOVEMENT_H
#define MOVEMENT_H

#include "soukoban.h"

/**
 * プレイヤーの移動が可能か判定する関数
 *
 * @param int 移動する配列のおける距離
 * @param FIELD 対象のフィールド
 * @param INFO ゲーム情報
 *
 * @return true: 移動可能, false: 不可能
 */
bool canMove(int, FIELD, INFO);

/**
 * プレイヤーの移動を行う関数
 * カーソルキー以外の入力がされた場合は何もしない
 *
 * @param int 入力されたキー
 * @param FIELD 対象のフィールド
 * @param INFO ゲーム情報
 */
void movePlayer(int, FIELD *, INFO);

#endif
