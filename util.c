#include "util.h"

char* concatDirAndFile(char *file) {
	char *filepath;

	filepath = malloc(sizeof(char) * (strlen(FIELD_DIR) + strlen(file) + 1));

	if (filepath == NULL) {
		endwin();
		printf("Cannot malloc pause filepath.\n");
		exit(EXIT_FAILURE);
	}

	sprintf(filepath, "%s%s", FIELD_DIR, file);

	return filepath;
}

void removePauseFile() {
	char *pausePath;

	pausePath = concatDirAndFile(PAUSE_FILE);
	remove(pausePath);
}
