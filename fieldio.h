#ifndef FIELDIO_H
#define FIELDIO_H

#include "soukoban.h"
#include "util.h"

/**
 * ポーズファイルが存在するかどうかを判定する関数
 *
 * @return true: 存在する, false: しない
 */
bool containsPauseFile();

/**
 * フィールドを読み込む関数
 *
 * @param FIELD* 対象のフィールド
 * @param INFO* ゲーム情報
 */
void loadNormalFile(FIELD *, INFO *);

/**
 * ポーズファイルのフィールドを読み込む関数
 * ポーズファイルと通常のフィールドファイルはフォーマットが違う。
 *
 * @param FIELD* 対象のフィールド
 * @param INFO* ゲーム情報
 */
void loadPauseFile(FIELD *, INFO *);

/**
 * ゲームを保存する関数
 * ポーズするときに呼ぶ。
 *
 * @param FIELD 対象のフィールド
 * @param INFO ゲーム情報
 */
void saveGame(FIELD, INFO);

#endif
