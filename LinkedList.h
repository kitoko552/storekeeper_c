#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "soukoban.h"

/**
 * 連結リストを実現するノード構造体
 */
typedef struct node {
	FIELD field;
	struct node *next;
} NODE, *LINKED_LIST;

/**
 * リストの先頭に要素を追加する関数
 *
 * @param FIELD 追加する値
 * @param LINKED_LIST* 対象の連結リストのポインタ
 *
 * @return 成功: true, 失敗: false
 */
bool push(FIELD, LINKED_LIST *);

/**
 * リストの先頭要素を削除し、取得する関数
 *
 * @param FIELD* 取得した先頭要素を受け取るポインタ
 * @param LINKED_LIST* 対象の連結リストのポインタ
 *
 * @return 成功: true, 失敗: false
 */
bool pop(FIELD *, LINKED_LIST *);

/**
 * リストの指定した位置の要素を削除する関数
 *
 * @param int 削除する位置
 * @param LINKED_LIST* 対象の連結リストのポインタ
 *
 * @return 成功: true, 失敗: false
 */
bool removeAt(int, LINKED_LIST *);

/**
 * リストの要素数を取得する関数
 *
 * @param LINKED_LIST 対象の連結リストのポインタ
 *
 * @return 要素数
 */
int size(LINKED_LIST);

/**
 * リストの全要素を削除する関数
 *
 * @param LINKED_LIST* 対象の連結リストのポインタ
 */
void clearAll(LINKED_LIST *);

#endif
