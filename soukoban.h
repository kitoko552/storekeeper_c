#ifndef SOUKOBAN_H
#define SOUKOBAN_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <dirent.h>
#include <ncurses.h>

#define PLAYER 'p'
#define WALL '#'
#define GOAL '.'
#define CARGO 'o'
#define SPACE ' '
#define PLAYER_ON_GOAL 'P'
#define CARGO_ON_GOAL 'O'

#define QUIT 'q'
#define RESET 'r'
#define UNDO 'u'
#define PAUSE 'p'
#define ENTER '\n'

#define FIELD_DIR "field/"
#define PAUSE_FILE ".pause"

typedef enum { NORMAL, STEPS } Mode; // ゲームモード

/**
 * 倉庫番のフィールドを実現する構造体
 */
typedef struct field {
	int playerPosition; // プレイヤーの現在地
	char *squares; // オブジェクトの配列（改行含む）
} FIELD;

/**
 * 現在プレイしているゲームの情報
 */
typedef struct gameInfo {
	Mode mode;
	char *filename; // フィールドファイルの名前
	int limitSteps; // 制限歩数
	int steps; // 現在歩数（基本最初は0 ポーズファイルのときのみ0以上）
	int rowSize; // 行のオブジェクト数
	int columnSize; // 列のオブジェクト数
} INFO;

#endif
