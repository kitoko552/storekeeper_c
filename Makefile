# Makefile

soukoban: game.o movement.o util.o fieldio.o setting.o linkedlist.o
	clang -o soukoban -lncurses game.o movement.o util.o fieldio.o setting.o linkedlist.o

# game
game.o: game.c
	clang -c game.c

game.o: game.h soukoban.h linkedlist.h fieldio.h util.h movement.h setting.h

# movement
movement.o: movement.c
	clang -c movement.c

movement.o: movement.h soukoban.h

# util
util.o: util.c
	clang -c util.c

util.o: util.h soukoban.h

# fieldio
fieldio.o: fieldio.c
	clang -c fieldio.c

fieldio.o: fieldio.h soukoban.h util.h

# setting
setting.o: setting.c
	clang -c setting.c

setting.o: setting.h soukoban.h fieldio.h

# linkedlist
linkedlist.o: linkedlist.c
	clang -c linkedlist.c

linkedlist.o: linkedlist.h


## clean
clean:
	rm -f game.o movement.o util.o fieldio.o setting.o linkedlist.o
