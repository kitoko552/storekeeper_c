#include "fieldio.h"

#define MAX_FILE_LENGTH 32

/**
 * フィールドファイルに指定したオブジェクトを表す文字以外の文字が入っていないか調べる関数
 *
 * @param int チェックする文字
 *
 * @return true: 入っている false: 入っていない
 */
bool containsGarbage(int);


bool containsPauseFile() {
	DIR *dp;
	struct dirent *dirst;
	int size = 0;

	dp = opendir(FIELD_DIR);

	if (dp == NULL) {
		endwin();
		printf("Cannot open directory.\n");
		closedir(dp);
		exit(EXIT_FAILURE);
	}

	while ((dirst = readdir(dp)) != NULL) {
		if (strcmp(PAUSE_FILE, dirst->d_name) == 0) {
			return true;
		}
	}

	return false;
}

void loadNormalFile(FIELD *field, INFO *info) {
	FILE *fp;
	int object;
	int i = 0;
	char *filepath;

	filepath = concatDirAndFile(info->filename);

	fp = fopen(filepath, "r");

	// もう使わないので解放する。
	free(filepath);
	filepath = NULL;

	if (fp == NULL) {
		endwin();
		printf("Cannot open %s.\n", info->filename);
		fclose(fp);
		exit(EXIT_FAILURE);
	}

	// 1行目の制限歩数を取得
	// 制限歩数を書いているかチェック
	if (fscanf(fp, "%d\n", &(info->limitSteps)) <= 0) {
		endwin();
		printf ("Input limit of steps on the head of %s\n", info->filename);
		fclose(fp);
		exit(EXIT_FAILURE);
	}

	// 2行目の行列数番号を取得
	// 行列数を書いているかチェック
	if (fscanf(fp, "%d %d\n", &(info->rowSize), &(info->columnSize)) <= 0) {
		endwin();
		printf ("Input field size on the second line of %s\n", info->filename);
		fclose(fp);
		exit(EXIT_FAILURE);
	}

	info->columnSize += 1; // 改行文字分プラス

	field->squares = malloc(sizeof(char) * (info->rowSize * info->columnSize + 1));

	if (field->squares == NULL) {
		endwin();
		printf("Cannot malloc.\n");
		fclose(fp);
		exit(EXIT_FAILURE);
	}

	// while(fget != EOF)でやらないのは、フィールドのテキストファイルに無駄な改行などがあった場合に対処するため
	for (i = 0; i < info->rowSize * info->columnSize; i++) {
		object = fgetc(fp);

		if (containsGarbage(object)) {
			endwin();
			printf("Field file has invalid object.\n");
			fclose(fp);
			exit(EXIT_FAILURE);
		}

		if (object == PLAYER || object == PLAYER_ON_GOAL) {
			field->playerPosition = i;
		}

		field->squares[i] = object;
	}

	// この時点でEOFでないのは、最初の行列数を間違えているか、余分な文字が入っているとき
	if ((object = fgetc(fp)) != EOF) {
		endwin();
		printf("Field file is invalid format.\n");
		fclose(fp);
		exit(EXIT_FAILURE);
	}

	fclose(fp);
}

void loadPauseFile(FIELD *field, INFO *info) {
	FILE *fp;
	int object;
	int i = 0;
	char *filepath; // fieldディレクトリとfilenameを連結した文字列
	char *tmpFilename;

	filepath = concatDirAndFile(PAUSE_FILE);

	fp = fopen(filepath, "r");

	// もう使わないので解放する。
	free(filepath);
	filepath = NULL;

	if (fp == NULL) {
		endwin();
		printf("Cannot open %s.\n", PAUSE_FILE);
		fclose(fp);
		exit(EXIT_FAILURE);
	}

	tmpFilename = realloc(info->filename, sizeof(char) * MAX_FILE_LENGTH);

	if (tmpFilename == NULL) {
		endwin();
		printf("Cannot realloc filename.\n");
		fclose(fp);
		exit(EXIT_FAILURE);
	}

	info->filename = tmpFilename;
	tmpFilename = NULL;

	// 1行目のファイル名を取得
	// 制限歩数を書いているかチェック
	if (fscanf(fp, "%s\n", info->filename) <= 0) {
		endwin();
		printf ("Input filename on the head of %s\n", PAUSE_FILE);
		fclose(fp);
		exit(EXIT_FAILURE);
	}

	// メモリ節約のため、ファイル名の長さ分にrealloc
	tmpFilename = realloc(info->filename, sizeof(char) * (strlen(info->filename) + 1));

	if (tmpFilename == NULL) {
		endwin();
		printf("Cannot realloc filename.\n");
		fclose(fp);
		exit(EXIT_FAILURE);
	}

	info->filename = tmpFilename;
	tmpFilename = NULL;

	// 2行目の制限歩数、歩数、モードを取得
	// 制限歩数を書いているかチェック
	if (fscanf(fp, "%d %d %d\n", &(info->limitSteps), &(info->steps), &(info->mode)) <= 0) {
		endwin();
		printf ("Input limit of steps on the second line of %s\n", PAUSE_FILE);
		fclose(fp);
		exit(EXIT_FAILURE);
	}

	// 3行目の行列数番号を取得
	// 行列数を書いているかチェック
	if (fscanf(fp, "%d %d\n", &(info->rowSize), &(info->columnSize)) <= 0) {
		endwin();
		printf ("Input field size on the third line of %s\n", PAUSE_FILE);
		fclose(fp);
		exit(EXIT_FAILURE);
	}

	info->columnSize += 1; // 改行文字分プラス

	field->squares = malloc(sizeof(char) * (info->rowSize * info->columnSize + 1));

	if (field->squares == NULL) {
		endwin();
		printf("Cannot malloc.\n");
		fclose(fp);
		exit(EXIT_FAILURE);
	}

	// while(fget != EOF)でやらないのは、フィールドのテキストファイルに無駄な改行などがあった場合に対処するため
	for (i = 0; i < info->rowSize * info->columnSize; i++) {
		object = fgetc(fp);

		if (containsGarbage(object)) {
			endwin();
			printf("Field file has invalid object.\n");
			fclose(fp);
			exit(EXIT_FAILURE);
		}

		if (object == PLAYER || object == PLAYER_ON_GOAL) {
			field->playerPosition = i;
		}

		field->squares[i] = object;
	}

	// この時点でEOFでないのは、最初の行列数を間違えているか、余分な文字が入っているとき
	if ((object = fgetc(fp)) != EOF) {
		endwin();
		printf("Field file is invalid format.\n");
		fclose(fp);
		exit(EXIT_FAILURE);
	}

	fclose(fp);
}

bool containsGarbage(int object) {
	return object != PLAYER
		&& object != WALL
		&& object != GOAL
		&& object != CARGO
		&& object != SPACE
		&& object != PLAYER_ON_GOAL
		&& object != CARGO_ON_GOAL
		&& object != '\n';
}

void saveGame(FIELD field, INFO info) {
	FILE *fp;
	char *filepath; // fieldディレクトリとfilenameを連結した文字列

	filepath = concatDirAndFile(PAUSE_FILE);

	fp = fopen(filepath, "w");

	// もう使わない
	free(filepath);
	filepath = NULL;

	if (fp == NULL) {
		endwin();
		printf("Cannot open pause file.\n");
		fclose(fp);
		exit(EXIT_FAILURE);
	}

	// -1は改行文字
	fprintf(fp, "%s\n%d %d %d\n%d %d\n%s", info.filename, info.limitSteps, info.steps, info.mode, info.rowSize, info.columnSize - 1, field.squares);
	fclose(fp);
}
