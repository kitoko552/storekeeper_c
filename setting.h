#ifndef SETTING_H
#define SETTING_H

#include "soukoban.h"
#include "fieldio.h"

/**
 * ゲーム情報を取得し、フィールドを設定する関数
 *
 * @param FIELD* 対象のフィールド
 * @param INFO* ゲーム情報
 */
void loadInfo(FIELD *, INFO *);

#endif
