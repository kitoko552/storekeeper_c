#include "movement.h"

/**
 * 移動先のオブジェクトが移動可能か判定する関数
 *
 * @param int 移動する配列のおける距離
 * @param FIELD 対象のフィールド
 *
 * @return true: 移動可能, false: 不可能
 */
bool canNextMove(int, FIELD);

/**
 * 移動先の一つ先のオブジェクトが移動可能か判定する関数
 *
 * @param int 移動する配列のおける距離
 * @param FIELD 対象のフィールド
 *
 * @return true: 移動可能, false: 不可能
 */
bool canStepMove(int, FIELD);

/**
 * 現在位置の移動を行う関数
 *
 * @param int 移動する配列のおける距離
 * @param FIELD* 対象のフィールド
 */
void currentMove(int, FIELD *);

/**
 * 移動先のオブジェクトの移動を行う関数
 *
 * @param int 移動する配列のおける距離
 * @param FIELD* 対象のフィールド
 */
void nextMove(int, FIELD *);

/**
 * 移動先の一つ先のオブジェクトの移動を行う関数
 * 移動先が荷物の場合に呼ばれる。
 *
 * @param int 移動する配列のおける距離
 * @param FIELD* 対象のフィールド
 */
void stepMove(int, FIELD *);


bool canMove(int inputChar, FIELD field, INFO info) {
	int distance;

	switch (inputChar) {
		case KEY_UP:
			distance = -(info.columnSize);
			break;

		case KEY_LEFT:
			distance = -1;
			break;

		case KEY_RIGHT:
			distance = 1;
			break;

		case KEY_DOWN:
			distance = info.columnSize;
			break;

		default:
			return false;
	}

	if (canNextMove(distance, field)) {
		return true;
	} else {
		return false;
	}
}

bool canNextMove(int distance, FIELD field) {
	int nextPosition = field.playerPosition + distance; //	プレイヤーの移動先の位置
	char nextObject = field.squares[nextPosition];

	switch (nextObject) {
		case WALL:
			return false;

		case CARGO:
		case CARGO_ON_GOAL:
			if (!canStepMove(distance, field)) {
				return false;
			}

			break;
	}

	return true;
}

bool canStepMove(int distance, FIELD field) {
	int stepPosition = (field.playerPosition) + distance * 2; // 荷物の移動先の位置
	char stepObject = field.squares[stepPosition];

	switch (stepObject) {
		case WALL:
		case CARGO:
		case CARGO_ON_GOAL:
			return false;
	}

	return true;
}

void movePlayer(int inputChar, FIELD *field, INFO info) {
	switch (inputChar) {
		case KEY_UP:
			currentMove(-(info.columnSize), field);
			break;

		case KEY_LEFT:
			currentMove(-1, field);
			break;

		case KEY_RIGHT:
			currentMove(1, field);
			break;

		case KEY_DOWN:
			currentMove(info.columnSize, field);
			break;
	}
}

void currentMove(int distance, FIELD *field) {
	int currentPosition = field->playerPosition;
	int nextPosition = field->playerPosition + distance; //	プレイヤーの移動先の位置

	char currentObject = field->squares[currentPosition]; // PLAYER or PLAYER_ON_GOAL

	switch (currentObject) {
		case PLAYER:
			nextMove(distance, field);
			field->squares[currentPosition] = SPACE;
			break;

		case PLAYER_ON_GOAL:
			nextMove(distance, field);
			field->squares[currentPosition] = GOAL;
			break;

		default:
			endwin();
			printf("error: 操作対象がプレイヤー以外になっています。\n");
			exit(EXIT_FAILURE);
	}

	field->playerPosition = nextPosition;
}

void nextMove(int distance, FIELD *field) {
	int nextPosition = field->playerPosition + distance; //	プレイヤーの移動先の位置
	char nextObject = field->squares[nextPosition];

	switch (nextObject) {
		case CARGO:
			stepMove(distance, field);
			field->squares[nextPosition] = PLAYER;
			break;

		case CARGO_ON_GOAL:
			stepMove(distance, field);
			field->squares[nextPosition] = PLAYER_ON_GOAL;
			break;

		case GOAL:
			field->squares[nextPosition] = PLAYER_ON_GOAL;
			break;

		case SPACE:
			field->squares[nextPosition] = PLAYER;
			break;

		default:
			endwin();
			printf("%c\n", nextObject);
			printf("error: フィールドに取り扱っていないオブジェクトが存在します。\n");
			exit(EXIT_FAILURE);
	}
}

void stepMove(int distance, FIELD *field) {
	int stepPosition = (field->playerPosition) + distance * 2; // 荷物の移動先の位置
	char stepObject = field->squares[stepPosition];

	switch (stepObject) {
		case GOAL:
			field->squares[stepPosition] = CARGO_ON_GOAL;
			break;

		case SPACE:
			field->squares[stepPosition] = CARGO;
			break;

		default:
			endwin();
			printf("error: フィールドに取り扱っていないオブジェクトが存在します。\n");
			exit(EXIT_FAILURE);
	}
}
