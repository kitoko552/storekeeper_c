#ifndef GAME_H
#define GAME_H

#include "soukoban.h"
#include "linkedlist.h"

#include "fieldio.h"
#include "util.h"
#include "movement.h"
#include "setting.h"

/**
 * 倉庫番ゲームを始める関数
 */
void playGame();

#endif
