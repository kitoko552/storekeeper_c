#include "setting.h"

#define MODE_NUM 2 // モード数を定義

/**
 * ゲームの始め方（はじめから or 続きから）を表示する関数
 * ポーズファイルがあるときのみ呼ばれる
 *
 * @return true: continueを選択, false: new gameを選択
 */
bool showSelectStart();

/**
 * フィールドを選択する画面を表示する関数
 *
 * @param char* ファイル名
 */
void showSelectField(char *);

/**
 * フィールドファイルが入っているディレクトリ内のファイルを表示する関数
 * 選択と同時にfilenameにファイル名を代入する。
 *
 * @param int 選択中のファイルのインデックス
 * @param char* ファイル名
 */
void showDir(int, char *);

/**
 * フィールドファイルが入っているディレクトリ内のファイル数を取得する関数
 */
int getDirSize();

/**
 * モードを選択する画面を表示する関数
 *
 * @param INFO* ゲーム情報
 */
void showSelectMode(INFO *);

/**
 * モードを表示する関数
 *
 * @param Mode 現在のモード
 */
void showMode(Mode);


void loadInfo(FIELD *field, INFO *info) {
	bool selectsContinue;
	char *pausePath;

	if (containsPauseFile()) {
		selectsContinue = showSelectStart();
	}

	if (selectsContinue) {
		loadPauseFile(field, info);
		removePauseFile();
	} else {
		removePauseFile();
		showSelectField(info->filename);
		showSelectMode(info);
		loadNormalFile(field, info);
	}
}

bool showSelectStart() {
	int key;
	char *tmpFilename;
	bool selectsContinue = false; // コンティニューを選んだかどうか

	while (true) {
		erase(); // 画面の消去
		addstr("Soukoban\n\n");

		if (selectsContinue) {
			addstr(" New Game\n");
			addstr(">Continue\n");
		} else {
			addstr(">New Game\n");
			addstr(" Continue\n");
		}

		// 二択しかないので移動は01で判断
		switch (key = getch()) {
			case QUIT:
				endwin();
				printf("Bye!\n");
				exit(EXIT_SUCCESS);
			case KEY_UP:
				if (selectsContinue) {
					selectsContinue = false;
				}

				break;
			case KEY_DOWN:
				if (!selectsContinue) {
					selectsContinue = true;
				}

				break;
			case ENTER:
				return selectsContinue;
		}
	}
}

void showSelectField(char *filename) {
	char ans;
	int key;
	int selectedIndex = 0;
	int dirSize;

	dirSize = getDirSize();

	while (true) {
		erase(); // 画面の消去
		addstr("Soukoban\n\n");
		addstr("Select field file.\n");
		showDir(selectedIndex, filename);

		switch (key = getch()) {
			case QUIT:
				endwin();
				printf("Bye!\n");
				exit(EXIT_SUCCESS);
			case KEY_UP:
				if (selectedIndex > 0) {
					selectedIndex--;
				}

				break;
			case KEY_DOWN:
				if (selectedIndex < dirSize - 1) {
					selectedIndex++;
				}

				break;
			case ENTER:
				return;
		}
	}
}

void showDir(int selectedIndex, char *filename) {
	DIR *dp;
	struct dirent *dirst;
	int i = 0;
	char *tmpFilename; // realloc用

	dp = opendir(FIELD_DIR);

	if (dp == NULL) {
		endwin();
		printf("Cannot open directory.\n");
		closedir(dp);
		exit(EXIT_FAILURE);
	}

	while ((dirst = readdir(dp)) != NULL) {
		// カレントディレクトリと親ディレクトリは表示しない
		if (strcmp(".", dirst->d_name) == 0 || strcmp("..", dirst->d_name) == 0) {
			continue;
		}

		if (i == selectedIndex) {
			printw(">%s\n", dirst->d_name);

			tmpFilename = realloc(filename, sizeof(char) * (strlen(dirst->d_name) + 1));

			if (tmpFilename == NULL) {
				endwin();
				printf("Cannot realloc filename.\n");
				exit(EXIT_FAILURE);
			}

			filename = tmpFilename;
			strcpy(filename, dirst->d_name);
		} else {
			printw(" %s\n", dirst->d_name);
		}

		i++;
	}

	closedir(dp);
}

int getDirSize() {
	DIR *dp;
	struct dirent *dirst;
	int size = 0;

	dp = opendir(FIELD_DIR);

	if (dp == NULL) {
		endwin();
		printf("Cannot open directory.\n");
		closedir(dp);
		exit(EXIT_FAILURE);
	}

	while ((dirst = readdir(dp)) != NULL) {
		// カレントディレクトリと親ディレクトリはカウントしない
		if (strcmp(".", dirst->d_name) == 0 || strcmp("..", dirst->d_name) == 0) {
			continue;
		}

		size++;
	}

	closedir(dp);

	return size;
}

void showSelectMode(INFO *info) {
	int key;

	info->mode = NORMAL;

	while (true) {
		erase(); // 画面の消去
		addstr("Soukoban\n\n");
		addstr("Select field file.\n");
		printw("%s\n\n", info->filename);
		addstr("Select mode.\n");
		showMode(info->mode);

		switch (key = getch()) {
			case QUIT:
				endwin();
				printf("Bye!\n");
				exit(EXIT_SUCCESS);
			case KEY_UP:
				if (info->mode > 0) {
					info->mode--;
				}

				break;
			case KEY_DOWN:
				if (info->mode < MODE_NUM - 1) {
					info->mode++;
				}

				break;
			case ENTER:
				return;
		}
	}
}

void showMode(Mode mode) {
	switch (mode) {
		case NORMAL:
			addstr(">NORMAL\n");
			addstr(" LIMIT STEPS\n");
			break;
		case STEPS:
			addstr(" NORMAL\n");
			addstr(">LIMIT STEPS\n");
			break;
	}

}
