#include "game.h"

#define CLEAR_MSG "Congratulations! You win!"
#define OVER_MSG "Game Over... You lose."

/**
 * 倉庫番ゲームがクリアしたかどうかを判定する関数
 *
 * @param FIELD 対象のフィールド
 * @param INFO ゲーム情報
 *
 * @return true: クリア, false: not クリア
 */
bool hasCleared(FIELD, INFO);

/**
 * ゲームオーバーになったかどうかを判定する関数
 *
 * @param INFO ゲーム情報
 *
 * @return true: ゲームオーバー, false: not ゲームオーバー
 */
bool isOver(INFO);

int main(void) {
	playGame();

	return 0;
}

void playGame() {
	FIELD field;
	LINKED_LIST list; // undo用の連結リスト
	INFO info;
	int key;
	bool isPlaying = true;

	// 初期値設定
	info.steps = 0;

	// 後でreallocする
	info.filename = malloc(sizeof(char));

	if (info.filename == NULL) {
		printf("Cannot malloc filename.\n");
		exit(EXIT_FAILURE);
	}

	initscr(); // 端末制御開始
	noecho();  // キー入力した文字の非表示モード
	cbreak(); // Enterキー入力不要モード
	curs_set(false); // カーソル非表示
	keypad(stdscr, true); // カーソルキーを有効化

	loadInfo(&field, &info);

	while (true) {
		erase(); // 画面の消去
		printw("MOVE: Keyboard QUIT: %c UNDO: %c RESET: %c PAUSE: %c\n", QUIT, UNDO, RESET, PAUSE);
		addstr(field.squares); // 文字列表示

		if (info.mode == STEPS) {
			printw("STEPS: %d - LIMIT: %d\n", info.steps, info.limitSteps);
		} else {
			printw("STEPS: %d\n", info.steps);
		}

		if (hasCleared(field, info)) {
			printw("\n%s\n", CLEAR_MSG);

			isPlaying = false;
			clearAll(&list);
		} else if (isOver(info)) {
			printw("\n%s\n", OVER_MSG);

			isPlaying = false;
			clearAll(&list);
		}

		switch (key = getch()) {
			case QUIT:
				endwin();
				removePauseFile();
				printf("Bye!\n");
				return;
			case RESET:
				loadNormalFile(&field, &info);
				clearAll(&list);
				info.steps = 0;
				isPlaying = true;
				break;
			case UNDO:
				if (pop(&field, &list)) {
					info.steps--;
				}

				break;
			case PAUSE:
				saveGame(field, info);
				endwin();
				printf("プレイデータを保存しました。\n");
				return;
		}

		// HACK: isPlaying気に食わない
		if (isPlaying) {
			if (canMove(key, field, info)) {
				push(field, &list);
				info.steps++;
				movePlayer(key, &field, info);
			}
		}
	}
}

bool hasCleared(FIELD field, INFO info) {
	int i;

	// フィールドに荷物（on goalでない）が存在すればfalse
	for (i = 0; i < info.rowSize * info.columnSize; i++) {
		if (field.squares[i] == CARGO) {
			return false;
		}
	}

	return true;
}

bool isOver(INFO info) {
	switch (info.mode) {
		case NORMAL:
			return false;
		case STEPS:
		return info.steps >= info.limitSteps;
	}
}
